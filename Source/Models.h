/*
  ==============================================================================

    Mixer.h
    Created: 3 Sep 2015 7:13:42pm
    Author:  yuasa

  ==============================================================================
*/

#ifndef MODELS_H_INCLUDED
#define MODELS_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

#include <mutex>
#include "./SoundTouch/include/SoundTouch.h"
#include "./SoundTouch/include/BPMDetect.h"

template<class Mutex>
std::unique_lock<Mutex> get_unique_lock(Mutex &mutex)
{
    return std::unique_lock<Mutex>(mutex);
}

class BPMDetector
{
public:
    BPMDetector();
  
    //! return negative value if bpm estimation is failed.
    double DetectBPM(juce::File target_file) const;
    
    double DetectBPM_impl(juce::AudioFormatReader *reader, double threshold) const;
};

class Filter
:   public juce::AudioSource
{
public:
    enum class Mode {
        kLowPass,
        kHighPass,
        kLowShelf,
        kHighShelf,
        kPeaking,
    };

    Filter();
    
    Mode GetFilterMode() const;
    void SetFilterMode(Mode mode);
    
    double GetSamplingRate() const;
    void SetSamplingRate(double sampling_rate);

    //! LowShelf, HighShelf, Peakingフィルターのレゾナンス値
    double GetQ() const;
    void SetQ(double Q);

    //! 正規化されていない実際の周波数
    double GetFrequency() const;
    void SetFrequency(double freq);
    
    //! LowShelf, HighShelf, PeakingフィルターのGain Factor
    double GetGainFactor() const;
    void SetGainFactor(double gain);
    
    //! inputSource will not be deleted by Filter.
    void SetInputAudioSource(AudioSource *inputSource);

    //! override
    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
    void releaseResources() override;
    void getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill) override;
    
private:
    double sampling_rate_;
    double Q_;
    double gain_;
    double filter_frequency_;
    juce::ScopedPointer<juce::IIRFilterAudioSource> filter_source_;
    Mode filter_mode_;
    
    juce::IIRCoefficients CalcCoeffs() const;
};

class Equalizer
:   public juce::AudioSource
{
private:
    Filter hi_1_;
    Filter hi_2_;
    Filter mid_;
    Filter lo_1_;
    Filter lo_2_;
    
    AudioSource *source_;
    bool deleteInputWhenDeleted_;
    std::mutex set_source_mutex_;
    int block_size_;
    int sampling_rate_;

public:
    Equalizer();
    
    //! LowShelf, HighShelf, PeakingフィルターのGain Factor
    double GetGain_High() const;
    void SetGain_High(double gain);
    
    double GetGain_Mid() const;
    void SetGain_Mid(double gain);

    double GetGain_Low() const;
    void SetGain_Low(double gain);
    
    void SetSamplingRate(double sampling_rate);
    double GetSamplingRate() const;
    
    void SetAudioSource(AudioSource *inputSource, bool deleteInputWhenDeleted);
    
    //! override
    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
    void releaseResources() override;
    void getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill) override;
};

struct TimeStretchedAudioSource
:   public juce::PositionableAudioSource
{
    TimeStretchedAudioSource();
    
    void    SetAudioSource(juce::PositionableAudioSource *inputSource, bool deleteInputWhenDeleted, int numChannels);
    
    void 	setNextReadPosition (int64 newPosition) override;
    int64 	getNextReadPosition () const override;
    
    int64 	getTotalLength () const override;
    bool 	isLooping () const override;
    void 	setLooping (bool shouldLoop) override;
    
    bool    IsPitchKeeped() const;
    void    KeepPitch(bool enable);
    
    //! GetPlayback Tempo
    float GetPlaybackTempo() const;
    
    //! Set Playback Tempo
    void SetPlaybackTempo(float tempo);

    //! @return the file tempo.
    float GetOriginalTempo();

    //! set file tempo.
    void SetOriginalTempo(float tempo);
    
    //! (1.0 + x)倍のスピードで再生。
    void AcceleratePlaybackSpeed(float x);
    float GetAcceleratedPlaybackSpeed() const;
    
    //! override
    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
    void releaseResources() override;
    void getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill) override;
    
private:
    juce::PositionableAudioSource *source_;
    bool deleteInputWhenDeleted_;

    juce::ScopedPointer<ResamplingAudioSource> resampler_;
    
    int num_channels_;
    double original_tempo_;
    double playback_tempo_;
    
    int sampling_rate_;
    int block_size_;
    
    int const kAccelUpdateInterval = 1024;
    int accel_update_after_;
    double accel_rate_;
    double playback_position_phase_;
    
    juce::AudioSampleBuffer input_buffer_;
    juce::AudioSourceChannelInfo input_buffer_channel_info_;
    
    bool keep_pitch_;
    
    int StretchedSamplePosToOriginalSamplePos(int pos) const;
    int OriginalSamplePosToStretchedSamplePos(int pos) const;
    
    void ApplyTempoAndRate();
    
    soundtouch::SoundTouch sound_touch_;
    std::vector<float> sound_touch_buffer_;
    std::mutex mutable accel_rate_mutex_;
};

struct Deck
:   public juce::AudioSource
{
    Deck();
    
    //! load a file into the deck
    //! @return false if failed.
    bool LoadAudioFile(juce::File audio_file_path);
    
    bool IsFileLoaded() const;
    juce::File GetLoadedFile() const;
    
    //! override
    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
    void releaseResources() override;
    void getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill) override;
    
    double GetPlaybackPosition() const;
    void SetPlaybackPosition(double pos);
    
    void Play();
    void Pause();
    bool IsPlaying() const;
    
    enum class TransportChangeStatus {
        kStopped
    };
    
    typedef std::function<void(TransportChangeStatus status)> change_transport_status_callback;
    
    void SetChangeTransportCallback(change_transport_status_callback callback);
    void ClearChangeTransportCallback();
    
    double  GetGain() const;
    void    SetGain(double gain);
    
    Equalizer & GetEqualizer();
    Equalizer const & GetEqualizer() const;
    
    //! (1.0 + x)倍のスピードで再生。
    void AcceleratePlaybackSpeed(float x);
    float GetAcceleratedPlaybackSpeed() const;
    
    //! GetPlayback Tempo
    float GetPlaybackTempo() const;
    
    //! Set Playback Tempo
    void SetPlaybackTempo(float tempo);
    
    //! @return the file tempo.
    float GetFileTempo();
    
    //! set file tempo.
    void SetFileTempo(float tempo);
    
private:
    change_transport_status_callback callback_;
    juce::File loaded_file_;
    juce::ScopedPointer<juce::AudioFormatReaderSource> file_source_;
    juce::ScopedPointer<juce::AudioTransportSource> transport_source_;
    int sampling_rate_;
    int block_size_;
    //std::atomic<bool> playing_;
    std::mutex mutable mutex_;
    double deck_gain_; // 0.0 .. 1.0 (linear, not dB)
    TimeStretchedAudioSource stretch_;
    
    Equalizer eq_;
};

class Effector
:   public juce::AudioSource
{
public:
    Effector();
    ~Effector();
    
    bool LoadPlugin(juce::File plugin_path);
    bool IsPluginLoaded() const;
    
    bool HasEditor() const;
    juce::AudioProcessorEditor * CreateEditor();
    
    juce::String GetPluginName() const;
    
    int GetProgramCount() const;
    juce::String GetProgramName(int index) const;
    void SetProgram(int index);
    
    int GetParameterCount() const;
    juce::String GetParameterName(int index) const;
    float GetParameterValue(int index) const;
    void SetParameterValue(int index, float value);
    
    void SetDryWetRatio(float ratio);
    double GetDryWetRatio() const;
    
public:
    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
    void releaseResources() override;
    void getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill) override;
    
private:
    juce::AudioSampleBuffer effect_buffer_;
    juce::ScopedPointer<juce::AudioProcessor> proc_;
    std::mutex proc_mutex_;
    int block_size_;
    int sampling_rate_;
    
    juce::AudioPluginFormatManager plugin_format_manager_;
    juce::KnownPluginList plugin_list_;
    
    double dry_wet_ratio_;
    double dry_gain_;
    double wet_gain_;
    double last_dry_gain_;
    double last_wet_gain_;
};

class Mixer
    :   public juce::AudioSource
{
public:
    Mixer();

    void SetDecks(Deck &deck_a, Deck &deck_b);
    
    //! Left(-1.0) .. Center(0.0) .. Right(1.0);
    double  GetMixingRatio() const;
    
    //! Left(-1.0) .. Center(0.0) .. Right(1.0);
    void    SetMixingRatio(double ratio);
    
    Effector & GetEffector();
    Effector const & GetEffector() const;
    
private:
    Deck    *deck_a_;
    Deck    *deck_b_;
    
    double mix_ratio_;
    MixerAudioSource mixer_;
    Effector effector_;
    
public:    
    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
    void releaseResources() override;
    void getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill) override;
};

#endif  // MODELS_H_INCLUDED
