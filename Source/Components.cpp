/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "Components.h"
#include "Models.h"
#include "Accessor.h"
#include "BinaryResource.h"

//! 線形な音量値からdBへの変換
//! 4.0 -> + 12dB
//! 2.0 -> +  6dB
//! 1.0 ->    0dB
//! 0.5 -> -  6dB
//! 0.25-> - 12dB
//! 0.0 -> -300dB
static double const dB_300 = pow(10.0, -300/20.0);

inline
double linear_to_dB(double linear)
{
    assert(linear >= 0);
    
    if(linear < dB_300) {
        return -300;
    } else {
        return 20.0 * log10(linear);
    }
}

//! dBから線形な音量値への変換
//!   +  12dB -> 4.0
//!   +   6dB -> 2.0
//!   +/- 0dB -> 1.0
//!   -   6dB -> 0.5
//!   -  12dB -> 0.25
//! < - 300dB -> 0.0
inline
double dB_to_linear(double dB)
{
    if(dB < -300) {
        return 0;
    } else {
        return pow(10.0, dB/20.0);
    }
}

void SetSliderColour(juce::Slider &slider)
{
    slider.setColour(juce::Slider::ColourIds::rotarySliderOutlineColourId, juce::Colours::white);
    slider.setColour(juce::Slider::ColourIds::rotarySliderFillColourId, juce::Colours::white);
}

void TempoSlider::SetOriginalTempo(double original_tempo)
{
    original_tempo_ = original_tempo;
    setRange(proportionOfLengthToValue(1.0), proportionOfLengthToValue(0));
    updateText();
}

double TempoSlider::proportionOfLengthToValue (double proportion)
{
    return original_tempo_ * pow(1.5, (1.0 - proportion) * 2 - 1.0);
}

double TempoSlider::valueToProportionOfLength (double value)
{
    //log_a(B) = log_c(B) / log_c(A)
    
    double const n = (value / original_tempo_);
    
    //log_(1.5)(n) = log_10(n)/log_10(c);
    
    static double const k = log10(1.5);
    
    double m = log10(n) / k;
    
    //assert(m >= -1.0 && m <= 1.0);
    
    if(m < -1.0) { m = -1.0; }
    if(m > 1.0) { m = 1.0; }
    
    return 1.0 - (m / 2 + 0.5);
}

TempoSliderComponent::TempoSliderComponent()
:   deck_(nullptr)
{
    addAndMakeVisible(tempo_slider_);
    tempo_slider_.addListener(this);
    tempo_slider_.SetOriginalTempo(120.0);
    tempo_slider_.setSliderStyle(juce::Slider::SliderStyle::LinearVertical);
}

void TempoSliderComponent::SetDeck(Deck &deck)
{
    deck_ = &deck;
    tempo_slider_.SetOriginalTempo(deck_->GetFileTempo());
}

void TempoSliderComponent::UpdateTempoValue()
{
    assert(deck_);
    tempo_slider_.SetOriginalTempo(deck_->GetFileTempo());
    tempo_slider_.setValue(deck_->GetPlaybackTempo(), juce::NotificationType::dontSendNotification);
}

void TempoSliderComponent::resized()
{
    int const text_box_width = 80;
    int const text_box_height = 20;
    
    tempo_slider_.setTextBoxStyle(juce::Slider::TextBoxAbove, false, text_box_width, text_box_height);
    tempo_slider_.setBounds(0, text_box_height, getWidth(), getHeight() - text_box_height);
}

void TempoSliderComponent::sliderValueChanged(Slider *slider)
{
    if(!deck_) { return; }
    deck_->SetPlaybackTempo(slider->getValue());
}



class BPMDetetedMessage
:   public juce::Message
{
public:
    BPMDetetedMessage(Deck *deck, juce::File file, double bpm)
    :   deck_(deck)
    ,   file_(file)
    ,   bpm_(bpm)
    {}
    
public:
    Deck *deck_;
    juce::File file_;
    double bpm_;
};

class TransportComponent::DeckTransportChangeMessage
:   public juce::Message
{
public:
    DeckTransportChangeMessage(Deck::TransportChangeStatus status)
    :   status_(status)
    {}
    
public:
    Deck::TransportChangeStatus status_;
};

TransportComponent::TransportComponent()
:   deck_(nullptr)
{
    addAndMakeVisible(play_button_);
    play_button_.addListener(this);
    play_button_.setButtonText("Play");
    
    play_button_.setClickingTogglesState(true);
    play_button_.setToggleState(false, juce::NotificationType::dontSendNotification);
    play_button_.setEnabled(false);
    
    auto fullpath = juce::File::getCurrentWorkingDirectory().getFullPathName();
    play_button_image_ = ImageCache::getFromMemory (BinaryResource::playback_button_png, BinaryResource::playback_button_pngSize);
    
    play_button_.setImages(false, true, true,
                           play_button_image_, 0.7, juce::Colours::transparentBlack,
                           play_button_image_, 1.0, juce::Colours::transparentBlack,
                           play_button_image_, 1.0, juce::Colours::lightgreen.withAlpha(0.8f),
                           0.5f);
}

void TransportComponent::resized()
{
    auto r = getLocalBounds();
    
    r = r.reduced(5);
    play_button_.setBounds(r.removeFromLeft(r.getHeight()));
}

void TransportComponent::SetDeck(Deck &deck)
{
    if(deck_) {
        deck_->ClearChangeTransportCallback();
    }
    deck_ = &deck;
    
    deck_->SetChangeTransportCallback(
                                [this](Deck::TransportChangeStatus status) {
                                    this->postMessage(new DeckTransportChangeMessage(status));
                                });
}

void TransportComponent::paint(juce::Graphics &g)
{}

void TransportComponent::buttonClicked(juce::Button *btn)
{
    if(btn == &play_button_) {
        if(play_button_.getToggleStateValue() == true) {
            deck_->Play();
        } else {
            deck_->Pause();
        }
    }
}

void TransportComponent::handleMessage(juce::Message const &msg)
{
    if(auto p = dynamic_cast<DeckTransportChangeMessage const *>(&msg)) {
        OnChangeTransport(p->status_);
    }
}

void TransportComponent::OnChangeTransport(Deck::TransportChangeStatus status)
{
    if(status == Deck::TransportChangeStatus::kStopped) {
        play_button_.setToggleState(false, juce::dontSendNotification);
    }
}

void TransportComponent::OnPlay()
{
    if(!deck_) { return; }
    deck_->Play();
}

void TransportComponent::OnFileLoad()
{
    play_button_.setEnabled(true);
}

TurnTableComponent::TurnTableComponent(bool left_hand_side)
:   deck_(nullptr)
,   left_hand_side_(left_hand_side)
{
    addAndMakeVisible(tempo_slider_);
    addAndMakeVisible(load_file_button_);
    addAndMakeVisible(file_path_label_);
    addAndMakeVisible(play_button_);
    addAndMakeVisible(waveform_);
    addAndMakeVisible(transport_);
    
    load_file_button_.setButtonText("Load");
    play_button_.setButtonText("Play");
    
    load_file_button_.addListener(this);
    play_button_.addListener(this);
    
    file_path_label_.setColour(juce::Label::ColourIds::textColourId, juce::Colours::white);
    file_path_label_.setColour(juce::Label::ColourIds::outlineColourId, juce::Colours::grey);
    
    record_ = ImageCache::getFromMemory (BinaryResource::record_png, BinaryResource::record_pngSize);
    record_stain_ = ImageCache::getFromMemory (BinaryResource::record_stain_png, BinaryResource::record_stain_pngSize);
    
    startTimer(16);
}

TurnTableComponent::~TurnTableComponent()
{
    if(bpm_detection_thread_.joinable()) {
        bpm_detection_thread_.join();
    }
}

void TurnTableComponent::SetDeck(Deck &deck)
{
    deck_ = &deck;
    tempo_slider_.SetDeck(deck);
    waveform_.SetDeck(deck);
    transport_.SetDeck(deck);
}

void TurnTableComponent::UpdateTempoSlider()
{
    tempo_slider_.UpdateTempoValue();
}

void TurnTableComponent::resized()
{
    int const w = this->getWidth();
    int const h = this->getHeight();
    
    int const waveform_height = h / 8;
    
    int const reducing_amount = 5;
    int const label_height = 20;
    int const header_height = label_height + reducing_amount *  2;
    
    int const load_button_width = 60;
    
    int const tempo_slider_width = h / 6;
    int const tempo_slider_height = h / 2;
    
    int const image_area_width = getWidth() - tempo_slider_width;
    int const transport_height = 60;
    
    auto header_bounds = getLocalBounds();
    header_bounds = header_bounds.removeFromTop(header_height);
    header_bounds = header_bounds.reduced(reducing_amount);
    
    auto const load_button_bounds = header_bounds.removeFromRight(load_button_width);
    
    file_path_label_.setBounds(header_bounds);
    load_file_button_.setBounds(load_button_bounds);
    
    auto waveform_bounds = getLocalBounds();
    waveform_bounds.removeFromTop(header_height);
    waveform_bounds = waveform_bounds.removeFromTop(waveform_height);
    waveform_.setBounds(waveform_bounds);
    
    auto transport_bounds = getLocalBounds();
    transport_bounds.removeFromTop(header_height + waveform_height + image_area_width);
    transport_bounds.setHeight(transport_height);
    transport_.setBounds(transport_bounds);
    
    if(left_hand_side_) {
        tempo_slider_.setBounds(0, h / 2, tempo_slider_width, tempo_slider_height);
        transport_bounds.removeFromLeft(tempo_slider_width);
        transport_.setBounds(transport_bounds);
    } else {
        tempo_slider_.setBounds(w - tempo_slider_width, h / 2, tempo_slider_width, tempo_slider_height);
        transport_.setBounds(transport_bounds);
    }
}

void TurnTableComponent::paint (Graphics& g)
{
    int const turntable_y = waveform_.getBottom();
    int const tempo_slider_width = tempo_slider_.getWidth();
    int const image_area_width = getWidth() - tempo_slider_width;
    
    juce::AffineTransform translate_disc;
    if(left_hand_side_) {
        translate_disc = translate_disc.translated(tempo_slider_width, turntable_y);
    } else {
        translate_disc = translate_disc.translated(0, turntable_y);
    }
    
    juce::AffineTransform t;
    
    juce::AffineTransform scaling;
    scaling = scaling.scaled((double)(image_area_width-1) / record_stain_.getWidth(), (double)(image_area_width-1) / record_stain_.getHeight());
    
    t = scaling;
    t = t.followedBy(translate_disc);
    
    g.drawImageTransformed(record_, t);
    
    t = t.identity;
    
    t = t.rotated(deck_->GetPlaybackPosition() * 2 * 3.141592653589793 * 100, record_stain_.getWidth() / 2, record_stain_.getHeight() / 2);
    t = t.followedBy(scaling);
    t = t.followedBy(translate_disc);
    
    g.drawImageTransformed(record_stain_, t);
    
    if(left_hand_side_) {
        g.setColour(juce::Colours::lightcyan);
    } else {
        g.setColour(juce::Colours::lightcyan);
    }
    
    g.setColour(juce::Colours::white);
    auto playback_position_bounds = getLocalBounds();
    playback_position_bounds.removeFromTop(turntable_y + image_area_width / 2);
    playback_position_bounds.setHeight(image_area_width / 2);
    
    if(left_hand_side_) {
        playback_position_bounds.removeFromLeft(tempo_slider_width);
    } else {
        playback_position_bounds.removeFromRight(tempo_slider_width);
    }
    
    auto string = juce::String::formatted(
                                          "Pos:   %f\n"
                                          "Accel: %f\n",
                                          deck_->GetPlaybackPosition(),
                                          deck_->GetAcceleratedPlaybackSpeed()
                                          );
    g.drawText(string, playback_position_bounds, juce::Justification::centred);
}

void TurnTableComponent::mouseDown(MouseEvent const &event)
{
    assert(deck_);
    if(event.x <= 30 && event.y <= 30) {
        deck_->SetPlaybackPosition(0.1);
    }
}

void TurnTableComponent::mouseWheelMove(juce::MouseEvent const &event, juce::MouseWheelDetails const &wheel)
{
    if(!IsOnRecord(event.x, event.y)) { return; }
    
    int const tempo_slider_width = tempo_slider_.getWidth();
    int const image_area_width = getWidth() - tempo_slider_width;
    
    int x = event.x;
    if(left_hand_side_) {
        x -= tempo_slider_width;
    }
    
    auto center = (x < image_area_width / 2);
    
    double d = wheel.deltaY;
    if(x < image_area_width / 2) { d = -d; }
    
    double kSensitivity = 1.0;
    double const kMoveMax = 0.5;
    double const kMoveMin = 0.1;
    if(d < -kMoveMax) { d = -kMoveMax; }
    if(d > kMoveMax) { d = kMoveMax; }
    if(-kMoveMin < d && d < 0) { d = -kMoveMin; }
    if(0 < d && d < kMoveMin) { d = kMoveMin; }
    
    deck_->AcceleratePlaybackSpeed(d * kSensitivity);
}

void TurnTableComponent::timerCallback()
{
    repaint();
}

bool TurnTableComponent::IsOnRecord(int x, int y)
{
    int const tempo_slider_width = tempo_slider_.getWidth();
    int const image_area_width = getWidth() - tempo_slider_width;
    int const radius = image_area_width / 2 - 4;
    
    if(left_hand_side_) {
        x -= tempo_slider_width;
    }
    y -= waveform_.getBottom();

    if(x < image_area_width && y < image_area_width) {
        int const xd = x - image_area_width / 2;
        int const yd = y - image_area_width / 2;
        return sqrt((double)xd * xd + yd * yd) <= radius;
    }
    
    return false;
}

void TurnTableComponent::buttonClicked(juce::Button *btn)
{
    if(btn == &load_file_button_) {
        LoadFile();
    } else if(btn == &play_button_) {
        Play();
    }
}

void 	TurnTableComponent::handleMessage (const Message &message)
{
    if(auto d = dynamic_cast<BPMDetetedMessage const *>(&message)) {
        if(d->deck_->GetLoadedFile() == d->file_) {
            d->deck_->SetFileTempo(d->bpm_);
            d->deck_->SetPlaybackTempo(d->bpm_);
            tempo_slider_.UpdateTempoValue();
        }
    }
}

void TurnTableComponent::LoadFile()
{
    juce::WildcardFileFilter wildcardFilter ("*.wav;*.aiff;*.mp3;*.mp4;*.m4a", String::empty, "Audio Files");
    
    auto const flags =
        juce::FileBrowserComponent::FileChooserFlags::canSelectFiles |
        juce::FileBrowserComponent::FileChooserFlags::openMode;
    
    
    juce::FileBrowserComponent browser (flags,
                                        juce::File::getSpecialLocation(juce::File::SpecialLocationType::userDesktopDirectory),
                                        &wildcardFilter,
                                        nullptr);
    juce::FileChooserDialogBox dialogBox ("Open some kind of file",
                                          "Please choose some kind of file that you want to open...",
                                          browser,
                                          false,
                                          Colours::lightgrey);
    if (dialogBox.show())
    {
        File selectedFile = browser.getSelectedFile (0);
        deck_->LoadAudioFile(selectedFile);
        if(deck_->IsFileLoaded()) {
            this->transport_.OnFileLoad();
            this->file_path_label_.setText(selectedFile.getFileName(), juce::NotificationType::dontSendNotification);
            this->waveform_.setFile(selectedFile);
            
            if(bpm_detection_thread_.joinable()) {
                bpm_detection_thread_.join();
            }
            
            bpm_detection_thread_ = std::thread([this, selectedFile] {
                BPMDetector d;
                double bpm = d.DetectBPM(selectedFile);
                
                if(bpm <= 0) {
                    bpm = 120;
                }
                
                if(std::fabs(std::floor(bpm) - bpm) < 0.2) {
                    bpm = std::floor(bpm);
                } else if(std::fabs(std::ceil(bpm) - bpm) < 0.2) {
                    bpm = std::ceil(bpm);
                }
                
                this->postMessage(new BPMDetetedMessage(deck_, selectedFile, bpm));
            });
        }
    }
}

void TurnTableComponent::Play()
{
    if(deck_->IsPlaying()) {
        deck_->Pause();
    } else {
        deck_->Play();
    }
}

DeckMixerComponent::DeckMixerComponent(std::function<void(double gain)> on_volume_changed)
:   deck_(nullptr)
,   on_volume_changed_(on_volume_changed)
{
    addAndMakeVisible(slider_hi_);
    addAndMakeVisible(slider_mid_);
    addAndMakeVisible(slider_lo_);
    addAndMakeVisible(channel_volume_);
    
    slider_hi_.addListener(this);
    slider_mid_.addListener(this);
    slider_lo_.addListener(this);
    channel_volume_.addListener(this);
    
    slider_hi_.setSliderStyle(juce::Slider::SliderStyle::RotaryVerticalDrag);
    slider_mid_.setSliderStyle(juce::Slider::SliderStyle::RotaryVerticalDrag);
    slider_lo_.setSliderStyle(juce::Slider::SliderStyle::RotaryVerticalDrag);
    channel_volume_.setSliderStyle(juce::Slider::SliderStyle::LinearVertical);
    
    slider_hi_.setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, false, 0, 0);
    slider_mid_.setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, false, 0, 0);
    slider_lo_.setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, false, 0, 0);
    channel_volume_.setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, false, 0, 0);
    
    slider_hi_.setRange(-1, 1);
    slider_mid_.setRange(-1, 1);
    slider_lo_.setRange(-1, 1);
    
    slider_hi_.setDoubleClickReturnValue(true, 0);
    slider_mid_.setDoubleClickReturnValue(true, 0);
    slider_lo_.setDoubleClickReturnValue(true, 0);
    channel_volume_.setRange(-48, 6);
    
    slider_hi_.setValue(0, juce::NotificationType::dontSendNotification);
    slider_mid_.setValue(0, juce::NotificationType::dontSendNotification);
    slider_lo_.setValue(0, juce::NotificationType::dontSendNotification);
    channel_volume_.setValue(-6, juce::NotificationType::dontSendNotification);
    
    SetSliderColour(slider_hi_);
    SetSliderColour(slider_mid_);
    SetSliderColour(slider_lo_);
}

void DeckMixerComponent::SetDeck(Deck &deck)
{
    deck_ = &deck;
}

double DeckMixerComponent::GetDeckVolume() const
{
    return dB_to_linear(channel_volume_.getValue());
}

void DeckMixerComponent::resized()
{
    int const w = getWidth();
    int const h = getHeight();
    
    double const knob_height = h / 6.0;
    double const knob_width = w;
    double const knob_size = std::min<double>(knob_height, knob_width);
    
    auto const center = [w,h](int index) { return juce::Point<int>(w / 2, (int)(h / 6.0 * (index + 0.5) + 0.5)); };

    juce::Rectangle<int> r(0, 0, round(knob_size), round(knob_size));
    auto hi_bounds = r.withCentre(center(0));
    auto mid_bounds = r.withCentre(center(1));
    auto lo_bounds = r.withCentre(center(2));

    r = getLocalBounds();
    auto volume_bounds = r.removeFromBottom(h / 2);
    
    slider_hi_.setBounds(hi_bounds);
    slider_mid_.setBounds(mid_bounds);
    slider_lo_.setBounds(lo_bounds);
    channel_volume_.setBounds(volume_bounds);
}

void DeckMixerComponent::paint(juce::Graphics &g)
{
    g.setColour(juce::Colours::white);
}

void DeckMixerComponent::sliderValueChanged(Slider *slider)
{
    assert(deck_);
    
    if(slider == &channel_volume_) {
        on_volume_changed_(GetDeckVolume());
    } else {
        double gain = pow(10.0, slider->getValue() / 2);
        
        if(slider == &slider_hi_) {
            deck_->GetEqualizer().SetGain_High(gain);
        } else if(slider == &slider_mid_) {
            deck_->GetEqualizer().SetGain_Mid(gain);
        } else if(slider == &slider_lo_) {
            deck_->GetEqualizer().SetGain_Low(gain);
        }
    }
}

MixerComponent::MixerComponent()
:   deck_mixer_a_([this] (double gain) { ApplyChannelVolume(gain, true); })
,   deck_mixer_b_([this] (double gain) { ApplyChannelVolume(gain, false); })
{
    addAndMakeVisible(deck_mixer_a_);
    addAndMakeVisible(deck_mixer_b_);
    addAndMakeVisible(channel_fader_);
    
    deck_mixer_a_.SetDeck(GetDeckA());
    deck_mixer_b_.SetDeck(GetDeckB());

    channel_fader_.addListener(this);
    channel_fader_.setSliderStyle(juce::Slider::SliderStyle::LinearHorizontal);
    
    channel_fader_.setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, false, 0, 0);

    channel_fader_.setRange(-1.0, 1.0);
    channel_fader_.setValue(0, juce::NotificationType::dontSendNotification);
}

void MixerComponent::resized()
{
    int const w = this->getWidth();
    int const h = this->getHeight();
    
    //! 上半分にEQ,
    //! 下半分にボリュームフェーダーとチャンネルフェーダー
    
    int const channel_fader_height = h / 10;
    
    auto deck_mixer_bounds_a = getBounds().withZeroOrigin();
    deck_mixer_bounds_a.removeFromBottom(channel_fader_height);
    auto deck_mixer_bounds_b = deck_mixer_bounds_a.removeFromRight(w / 2);
    
    auto channel_fader_bounds = getBounds().withZeroOrigin();
    channel_fader_bounds = channel_fader_bounds.removeFromBottom(channel_fader_height);
    
    deck_mixer_a_.setBounds(deck_mixer_bounds_a.reduced(5));
    deck_mixer_b_.setBounds(deck_mixer_bounds_b.reduced(5));
    channel_fader_.setBounds(channel_fader_bounds.reduced(5));
}

void MixerComponent::paint(juce::Graphics &g)
{
    g.setColour(juce::Colours::white);
}

void MixerComponent::sliderValueChanged (Slider* slider)
{
    if(slider == &channel_fader_) {
        GetDeckA().SetGain(CalcDeckAVolume());
        GetDeckB().SetGain(CalcDeckBVolume());
    }
}

double MixerComponent::CalcDeckAVolume()
{
    //! channel faderの値を左端を1, 右端を0に変換して、
    double const normalized_fader_value = channel_fader_.getValue() * (0.5) + 0.5;
    double const fader_gain = cos(normalized_fader_value * 0.5 * juce::float_Pi);
    return deck_mixer_a_.GetDeckVolume() * fader_gain;
}

double MixerComponent::CalcDeckBVolume()
{
    //! channel faderの値を右端を1, 左端を0に変換して、
    double const normalized_fader_value = channel_fader_.getValue() * (-0.5) + 0.5;
    double const fader_gain = cos(normalized_fader_value * 0.5 * juce::float_Pi);
    return deck_mixer_b_.GetDeckVolume() * fader_gain;
}

void MixerComponent::ApplyChannelVolume(double gain, bool is_deck_a)
{
    if(is_deck_a) {
        GetDeckA().SetGain(CalcDeckAVolume());
    } else {
        GetDeckB().SetGain(CalcDeckBVolume());
    }
}

XYPanelComponent::XYPanelComponent()
:   owner_(nullptr)
,   x_pos_(0.5)
,   y_pos_(0.5)
,   dragging_(false)
,   knob_width_(10)
{}

void XYPanelComponent::SetOwnerComponent(EffectorComponent &owner)
{
    owner_ = &owner;
}

void XYPanelComponent::SetX(float pos)
{
    x_pos_ = pos;
    repaint();
}

void XYPanelComponent::SetY(float pos)
{
    y_pos_ = pos;
    repaint();
}

void XYPanelComponent::mouseDrag(MouseEvent const &event)
{
    if(dragging_) {
        auto x_diff = event.getOffsetFromDragStart();
        x_pos_ = original_x_pos_ + (x_diff.x / ((double)getWidth() - knob_width_));
        y_pos_ = original_y_pos_ + (x_diff.y / ((double)getHeight() - knob_width_));
        
        if(x_pos_ < 0) { x_pos_ = 0; } else if(x_pos_ > 1.0) { x_pos_ = 1.0; }
        if(y_pos_ < 0) { y_pos_ = 0; } else if(y_pos_ > 1.0) { y_pos_ = 1.0; }
        
        owner_->OnValueChanged(x_pos_, 1.0 - y_pos_);
        repaint();
    }
}

void XYPanelComponent::mouseDown(MouseEvent const &event)
{
    auto const knob_x = x_pos_ * (getWidth() - knob_width_);
    auto const knob_y = y_pos_ * (getHeight() - knob_width_);
    
    auto const mx = event.x;
    auto const my = event.y;
    
    if(knob_x <= mx && mx <= knob_x + knob_width_ &&
       knob_y <= my && my <= knob_y + knob_width_ )
    {
        dragging_ = true;
        original_x_pos_ = x_pos_;
        original_y_pos_ = y_pos_;
        repaint();
    }
}

void XYPanelComponent::mouseUp(MouseEvent const &event)
{
    dragging_ = false;
    repaint();
}

void XYPanelComponent::paint(juce::Graphics &g)
{
    g.fillAll (Colour (0xfb061B16));
    
    if(owner_->IsPluginLoaded()) {
        int const w = getWidth();
        int const h = getHeight();
        
        int const x = x_pos_ * (w - knob_width_);
        int const y = y_pos_ * (h - knob_width_);
        
        g.setColour(juce::Colours::lightgreen.withAlpha(0.7f));
        g.drawLine(0, h / 2.0, w, h / 2.0);
        g.drawLine(w / 2.0, 0, w / 2.0, h);
        
        g.setColour(juce::Colours::coral);
        g.drawEllipse(x, y, knob_width_, knob_width_, 2);
        if(dragging_) {
            g.fillEllipse(x, y, knob_width_, knob_width_);
        }
    } else {
        g.setColour(juce::Colours::white);
        g.drawText("Drop Plugin Here", getLocalBounds(), juce::Justification::centred);
    }
}

void XYPanelComponent::resized()
{
    
}

bool XYPanelComponent::isInterestedInFileDrag (const StringArray &files)
{
    for(auto const &file: files) {
        if(juce::File(file).getFileExtension() != ".vst" &&
           juce::File(file).getFileExtension() != ".vst3" )
        {
            return false;
        }
    }
    
    return true;
}

void XYPanelComponent::filesDropped (const StringArray &files, int x, int y)
{
    if(files.size() == 0) { return; }
    
    owner_->OnPluginDropped(juce::File(files[0]));
    repaint();
}

PluginEditorWindow::PluginEditorWindow(juce::String plugin_name)
:   juce::DocumentWindow(plugin_name, juce::Colours::black, juce::DocumentWindow::TitleBarButtons::closeButton, true)
{
    setUsingNativeTitleBar (true);
}

PluginEditorWindow::~PluginEditorWindow()
{}

void PluginEditorWindow::SetEditor(juce::AudioProcessorEditor *editor)
{
    clearContentComponent();
    if(editor) {
        setContentOwned(editor, true);
    }
}

void PluginEditorWindow::closeButtonPressed()
{
    sendSynchronousChangeMessage();
    delete this;
}

EffectorComponent::EffectorComponent()
:   plugin_editor_window_()
{
    addAndMakeVisible(programs_list_);
    addAndMakeVisible(x_parameter_list_);
    addAndMakeVisible(y_parameter_list_);
    addAndMakeVisible(dry_wet_slider_);
    addAndMakeVisible(xy_panel_);
    addAndMakeVisible(open_editor_);
    
    open_editor_.addListener(this);
    xy_panel_.SetOwnerComponent(*this);
    
    open_editor_.setButtonText("Open Editor");
    open_editor_.setState(juce::Button::ButtonState::buttonNormal);
    
    open_editor_.setClickingTogglesState(true);
    open_editor_.setToggleState(false, juce::dontSendNotification);
    open_editor_.setEnabled(false);
    
    programs_list_.addListener(this);
    x_parameter_list_.addListener(this);
    y_parameter_list_.addListener(this);
    
    dry_wet_slider_.addListener(this);
    dry_wet_slider_.setRange(0.0, 1.0);
    dry_wet_slider_.setValue(0, juce::NotificationType::dontSendNotification);
    dry_wet_slider_.setSliderStyle(juce::Slider::SliderStyle::RotaryVerticalDrag);
    dry_wet_slider_.setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, false, 0, 0);
    
    SetSliderColour(dry_wet_slider_);
}

EffectorComponent::~EffectorComponent()
{
    if(plugin_editor_window_) {
        RemoveEditor();
    }
}

void EffectorComponent::OnValueChanged(float x, float y)
{
    if(x_parameter_list_.getSelectedId() != 0) {
        GetEffector().SetParameterValue(x_parameter_list_.getSelectedId()-1, x);
    }
    
    if(y_parameter_list_.getSelectedId() != 0) {
        GetEffector().SetParameterValue(y_parameter_list_.getSelectedId()-1, y);
    }
}

void EffectorComponent::OnPluginDropped(juce::File plugin_path)
{    
    RemoveEditor();
    
    auto &eff = GetEffector();
    bool const loaded = eff.LoadPlugin(plugin_path);
    if(!loaded) { return; }
    
    programs_list_.clear();
    x_parameter_list_.clear();
    y_parameter_list_.clear();
    
    if(eff.HasEditor()) {
        open_editor_.setEnabled(true);
        
        for(int i = 0; i < eff.GetProgramCount(); ++i) {
            auto name = eff.GetProgramName(i);
            if(name == "") { name = juce::String::formatted("Program - %02d", i); }
            
            programs_list_.addItem(name, i+1);
        }
        
        for(int i = 0; i < eff.GetParameterCount(); ++i) {
            auto name = eff.GetParameterName(i);
            if(name == "") {
                if(eff.GetParameterCount() < 100) {
                    name = juce::String::formatted("Parameter - %02d", i);
                } else {
                    name = juce::String::formatted("Parameter - %03d", i);
                }
            }
            x_parameter_list_.addItem(name, i+1);
            y_parameter_list_.addItem(name, i+1);
        }
    }
}

bool EffectorComponent::IsPluginLoaded() const
{
    return GetEffector().IsPluginLoaded();
}

void EffectorComponent::paint(juce::Graphics &g)
{
    g.setFont (Font (16.0f));
    g.setColour (Colours::white);
    
    auto r = getLocalBounds().reduced(5);
    r.setHeight(20);
    g.drawText("Program", r, juce::Justification::left, true);
    r.translate(0, 40);
    g.drawText("X Parameter:", r, juce::Justification::left, true);
    r.translate(0, 40);
    g.drawText("Y Parameter:", r, juce::Justification::left, true);
    
    r.setY(dry_wet_slider_.getY());
    r.setWidth(r.getWidth() - dry_wet_slider_.getWidth());
    r.setHeight(dry_wet_slider_.getHeight());
    r = r.reduced(5);
    g.drawText("Dry/Wet", r, juce::Justification::right | juce::Justification::verticallyCentred, true);
}

void EffectorComponent::resized()
{
    auto rect = getLocalBounds().reduced(5);
    int const kDryWetSliderWidth = rect.getWidth() / 3;
    
    rect.removeFromTop(20);
    programs_list_.setBounds(rect.removeFromTop(20));
    rect.removeFromTop(20);
    x_parameter_list_.setBounds(rect.removeFromTop(20));
    rect.removeFromTop(20);
    y_parameter_list_.setBounds(rect.removeFromTop(20));
    xy_panel_.setBounds(rect.removeFromTop(rect.getWidth()));
    open_editor_.setBounds(rect.removeFromTop(20));
    rect = rect.removeFromRight(kDryWetSliderWidth);
    dry_wet_slider_.setBounds(rect.removeFromTop(kDryWetSliderWidth));
}

void EffectorComponent::buttonClicked(juce::Button *button)
{
    if(button != &open_editor_) { return; }
    
    bool const kStateToOpen = true;
    if(open_editor_.getToggleState() == kStateToOpen) {
        plugin_editor_window_ = new PluginEditorWindow(GetEffector().GetPluginName());
        plugin_editor_window_->addChangeListener (this);
        plugin_editor_window_->SetEditor(GetEffector().CreateEditor());
        plugin_editor_window_->setVisible(true);
    } else {
        RemoveEditor();
    }
}

void EffectorComponent::changeListenerCallback (ChangeBroadcaster *source)
{
    if(source == plugin_editor_window_) {
        RemoveEditor();
        open_editor_.setToggleState(false, juce::dontSendNotification);
    }
}

void EffectorComponent::RemoveEditor()
{
    if(plugin_editor_window_) {
        plugin_editor_window_->setVisible(false);
        plugin_editor_window_->removeChangeListener(this);
        plugin_editor_window_.release();
    }
}

void EffectorComponent::comboBoxChanged (ComboBox *comboBoxThatHasChanged)
{
    if(comboBoxThatHasChanged == &programs_list_) {
        GetEffector().SetProgram(programs_list_.getSelectedId()-1);
    } else if(comboBoxThatHasChanged == &x_parameter_list_) {
        xy_panel_.SetX(GetEffector().GetParameterValue(x_parameter_list_.getSelectedId()-1));
    } else if(comboBoxThatHasChanged == &y_parameter_list_) {
        xy_panel_.SetY(GetEffector().GetParameterValue(y_parameter_list_.getSelectedId()-1));
    }
}

void EffectorComponent::sliderValueChanged(Slider *slider)
{
    if(slider == &dry_wet_slider_) {
        GetEffector().SetDryWetRatio(dry_wet_slider_.getValue());
    }
}

//==============================================================================
MainContentComponent::MainContentComponent()
:   turn_table_a_(true)
,   turn_table_b_(false)
,   effector_(new EffectorComponent())
{
    addAndMakeVisible(turn_table_a_);
    addAndMakeVisible(turn_table_b_);
    turn_table_a_.SetDeck(GetDeckA());
    turn_table_b_.SetDeck(GetDeckB());
    
    addAndMakeVisible(mixer_);
    
    addAndMakeVisible(effector_);
    
    setSize (920, 480);
}

MainContentComponent::~MainContentComponent()
{
}

void MainContentComponent::PrepareToShutdown()
{
   effector_ = nullptr;
}

void MainContentComponent::paint (Graphics& g)
{
    g.fillAll (Colour (0xff001F36));
}

void MainContentComponent::resized()
{
    int const w = getWidth();
    int const h = getHeight();
    int const turn_table_width = w / 3;
    int const mixer_width = w / 6;
    int const effector_width = w - (turn_table_width * 2 + mixer_width);
    
    turn_table_a_.setBounds(0, 0, turn_table_width, h);
    mixer_.setBounds(turn_table_width, 0, mixer_width, h);
    turn_table_b_.setBounds(turn_table_width + mixer_width, 0, turn_table_width, h);
    effector_->setBounds(w - effector_width, 0, effector_width, h);
}

