/*
  ==============================================================================

    Mixer.cpp
    Created: 3 Sep 2015 7:13:42pm
    Author:  yuasa

  ==============================================================================
*/

#include "Models.h"

template<class T>
struct AudioBuffer
{
    typedef T value_type;
    
    AudioBuffer(int num_channels, int length, bool is_interleaved)
    :   num_channels_(0)
    ,   length_(0)
    {
        resize(num_channels, length, is_interleaved);
    }
    
    void resize(int num_channels, int length, bool is_interleaved)
    {
        auto tmp_linear_buffer = std::vector<T>(num_channels * length);
        auto tmp_buffer_headers = std::vector<T *>(num_channels);
        
        if(is_interleaved) {
            for(auto &head: tmp_buffer_headers) {
                head = nullptr;
            }
        } else {
            for(int ch = 0; ch < num_channels; ++ch) {
                tmp_buffer_headers[ch] = tmp_linear_buffer.data() + (ch * length);
            }
        }
        
        std::swap(linear_buffer_, tmp_linear_buffer);
        std::swap(buffer_headers_, tmp_buffer_headers);
        num_channels_ = num_channels;
        length_ = length;
        is_interleaved_ = is_interleaved;
    }

    int get_num_channels() const { return num_channels_; }
    int get_length() const { return length_; }
    
    bool is_interleaved() const { return is_interleaved_; }

    void interleave(bool to_enable)
    {
        if(to_enable && !is_interleaved_) {
            auto copy = linear_buffer_;

            for(int ch = 0; ch < num_channels_; ++ch) {
                for(int smp = 0; smp < length_; ++smp) {
                    linear_buffer_[smp * num_channels_ + ch] = copy[ch * length_ + smp];
                }
                
                buffer_headers_[ch] = nullptr;
            }
            is_interleaved_ = true;
        } else if(!to_enable && is_interleaved_) {
            auto copy = linear_buffer_;
            
            for(int ch = 0; ch < num_channels_; ++ch) {
                for(int smp = 0; smp < length_; ++smp) {
                    linear_buffer_[ch * length_ + smp] = copy[smp * num_channels_ + ch];
                }
                
                buffer_headers_[ch] = linear_buffer_.data() + (ch * length_);
            }
            is_interleaved_ = false;
        }
    }

    T **    parallel_data() { return buffer_headers_.data(); }
    T *     interleaved_data() { return is_interleaved_ ? linear_buffer_.data() : nullptr; }
    T const * const *   parallel_data() const { return buffer_headers_.data(); }
    T const *           interleaved_data() const { return is_interleaved_ ? linear_buffer_.data() : nullptr; }
    
private:
    std::vector<T> linear_buffer_;
    std::vector<T *> buffer_headers_;
    bool is_interleaved_;
    int num_channels_;
    int length_;
};

struct BPMDetector_Beatport
{
    struct History
    {
        std::vector<double> values_;
        int data_index_;
        double total_;
        
        History(int size)
        :   values_(size)
        ,   data_index_(0)
        ,   total_(0)
        {}
        
        void push(double value)
        {
            total_ -= values_[data_index_];
            total_ += value;
            data_index_ = (data_index_ + 1) % values_.size();
            values_[data_index_] = value;
        }
        
        double get_avg() const
        {
            return total_ / values_.size();
        }
    };
    
    struct PeakInfo
    {
        PeakInfo() : position_(0), value_(0) {}
        PeakInfo(int position, double value) : position_(position), value_(value) {}
        
        int position_;
        double value_;
    };
    
    BPMDetector_Beatport(int numChannels, int sampleRate, double threshold)
    :   history_(1)
    {
        filter1_.setCoefficients(juce::IIRCoefficients::makeLowPass(sampleRate, 500));
        filter2_.setCoefficients(juce::IIRCoefficients::makeLowPass(sampleRate, 500));


        decimation_step_ = sampleRate / 500;
        sampling_rate_ = sampleRate;
        channels_ = numChannels;
        buffer_index_ = 0;
        decimated_buffer_index_ = 0;
        skip_ = 0;
        buffer_.resize(2048);
        decimated_buffer_.resize(256);
        process_pos_ = 0;
        threshold_ = threshold;
    }
    
    void InputSamples(float const * const *input, int length)
    {
        int const kChunkSize = 1024;
        for(int smp = 0; smp < length; ++smp) {
            float sum = 0;
            for(int ch = 0; ch < channels_; ++ch) {
                sum += input[ch][smp];
            }
            
            buffer_[buffer_index_] = sum / channels_;
            buffer_index_++;
            
            if(buffer_index_ == kChunkSize) {
                process(buffer_.data(), kChunkSize);
                buffer_index_ = 0;
            }
        }
    }

    //! 前後のバッファを

    void process(float *input, int length)
    {
        filter1_.processSamples(input, length);
        filter2_.processSamples(input, length);

        for(int i = 0; i < length; ++i) {
            history_.push(sqrt(input[i] * input[i]));

            if(skip_ != 0) {
                skip_ -= 1;
                continue;
            }

            if(history_.get_avg() >= threshold_) {
                peaks_.push_back(PeakInfo(i + process_pos_, history_.get_avg()));
                skip_ = 10000;
            }
        }

        process_pos_ += length;
    }

    
    struct IntervalCountInfo
    {
        IntervalCountInfo()
        :   interval_time_(0)
        ,   count_(0)
        {}
        
        IntervalCountInfo(int interval_time, int count)
        :   interval_time_(interval_time)
        ,   count_(count)
        {}
        
        int interval_time_;
        int count_;
    };
    
    std::vector<IntervalCountInfo>
        countIntervals(PeakInfo const *info, int num_info) const
    {
        std::vector<IntervalCountInfo> ret;
        for(int in = 0; in < num_info - 11; ++in) {
            for(int i = 1; i < 10; ++i) {
                int interval = info[i + in].position_ - info[in].position_;
                auto beat_per_sec = sampling_rate_ / (double)interval;
                auto as_bpm = 60.0 * beat_per_sec;
                auto aligned_bpm = (int)(as_bpm * 100 + 0.5) / 100.0;
                auto sec_per_beat = 60.0 / aligned_bpm;
                interval = (int)(sec_per_beat * sampling_rate_ + 0.5);
                bool processed = false;
                std::for_each(ret.begin(), ret.end(),
                              [interval, &processed](IntervalCountInfo &info) {
                                  if(interval == info.interval_time_) {
                                      info.count_ += 1;
                                      processed = true;
                                  }
                              });
                
                if(!processed) {
                    assert(interval > 0);
                    ret.push_back(IntervalCountInfo(interval, 1));
                }
            }
        }
        
        return ret;
    }
    
    struct TempoCountInfo
    {
        TempoCountInfo()
        :   tempo_(0)
        ,   count_(0)
        {}
        
        TempoCountInfo(double tempo, int count)
        :   tempo_(tempo)
        ,   count_(count)
        {}
        
        double tempo_;
        int count_;
    };
    
    std::vector<TempoCountInfo> listUpTempoCandidate(std::vector<IntervalCountInfo> const &infos) const
    {
        std::vector<TempoCountInfo> ret;
        for(auto const &info: infos) {
            double theoriticalTempo = 60 / (info.interval_time_ / (double)sampling_rate_);

            while(theoriticalTempo < 90) { theoriticalTempo *= 2; }
            while(theoriticalTempo > 180) { theoriticalTempo /= 2; }
            theoriticalTempo = (int)(theoriticalTempo * 100.0 + 0.5) / 100.0;
            
            bool processed = false;
            std::for_each(ret.begin(), ret.end(),
                          [theoriticalTempo, &processed] (TempoCountInfo &tempoInfo) {
                              if(std::fabs(tempoInfo.tempo_ - theoriticalTempo) < 0.02) {
                                  tempoInfo.count_++;
                                  processed = true;
                              }
                          });
            if(!processed) {
                ret.push_back(TempoCountInfo(theoriticalTempo, info.count_));
            }
        }
        
        return ret;
    }
    
    double detectBPM() const
    {
        auto interval_count_infos = countIntervals(peaks_.data(), peaks_.size());
        auto tempo_list = listUpTempoCandidate(interval_count_infos);
        if(tempo_list.empty()) {
            return 0;
        }
        std::sort(tempo_list.begin(), tempo_list.end(),
                  [](TempoCountInfo const &lhs, TempoCountInfo const &rhs) {
                      return lhs.count_ < rhs.count_;
                  });
        
        if(tempo_list.size() > 2 && (tempo_list.end()-1)->count_ > (tempo_list.end()-2)->count_ * 1.7) {
            return tempo_list.back().tempo_;
        } else {
            return 0;
        }
    }
    
    std::vector<PeakInfo> peaks_;
    
    History history_;
    int process_pos_;
    int skip_;
    int channels_;
    int sampling_rate_;
    std::vector<float> buffer_;
    std::vector<float> decimated_buffer_;
    int buffer_index_;
    int decimated_buffer_index_;
    juce::IIRFilter filter1_;
    juce::IIRFilter filter2_;
    double decimation_step_;
    double decimation_position_;
    double threshold_;
};

BPMDetector::BPMDetector()
{}

double BPMDetector::DetectBPM_impl(juce::AudioFormatReader *reader, double threshold) const
{
    int const kChunkSize = 2048;
    AudioBuffer<float> buffer(reader->numChannels, kChunkSize, false);
    
    BPMDetector_Beatport detector(reader->numChannels, reader->sampleRate, threshold);
    
    int position = 0;
    for( ; ; ) {
        int const num_samples_to_read = std::min<int>(position + kChunkSize, reader->lengthInSamples) - position;
        if(num_samples_to_read == 0) { break; }
        
        buffer.interleave(false);
        reader->read(reinterpret_cast<int **>(buffer.parallel_data()), reader->numChannels, position, num_samples_to_read, false);
        
        if(!reader->usesFloatingPointData) {
            for(int ch = 0; ch < reader->numChannels; ++ch) {
                for(int smp = 0; smp < num_samples_to_read; ++smp) {
                    //! Int to float
                    buffer.parallel_data()[ch][smp] = *reinterpret_cast<int const *>(&buffer.parallel_data()[ch][smp]) / (double)(0x80000000u);
                }
            }
        }
        
        detector.InputSamples(buffer.parallel_data(), num_samples_to_read);
        position += num_samples_to_read;
    }
    
    double const whole = detector.detectBPM();
    return whole;
}

double BPMDetector::DetectBPM(juce::File target_file) const
{
    juce::AudioFormatManager mgr;
    mgr.registerBasicFormats();
    juce::ScopedPointer<juce::AudioFormatReader> reader = mgr.createReaderFor(target_file);
    if(!reader) { return -1; }
    
    double threshold = 0.9;
    for( ; ; ) {
        if(threshold < 0.2) { return 0; }
        double const bpm = DetectBPM_impl(reader.get(), threshold);
        if(bpm == 0) {
            threshold *= 0.95;
        } else {
            return bpm;
        }
    }
}

Filter::Filter()
:   sampling_rate_(44100)
,   Q_(1.0)
,   gain_(1.0)
,   filter_frequency_(1000)
,   filter_mode_(Mode::kLowPass)
{
}

Filter::Mode Filter::GetFilterMode() const
{
    return filter_mode_;
}

void Filter::SetFilterMode(Mode mode)
{
    filter_mode_ = mode;
}

double Filter::GetSamplingRate() const
{
    return sampling_rate_;
}

void Filter::SetSamplingRate(double sampling_rate)
{
    sampling_rate_ = sampling_rate;
    if(filter_source_) {
        filter_source_->setCoefficients(CalcCoeffs());
    }
}

double Filter::GetQ() const
{
    return Q_;
}

void Filter::SetQ(double Q)
{
    Q_ = Q;
    if(filter_source_) {
        filter_source_->setCoefficients(CalcCoeffs());
    }
}

//! 正規化されていない実際の周波数
double Filter::GetFrequency() const
{
    return filter_frequency_;
}

void Filter::SetFrequency(double freq)
{
    filter_frequency_ = freq;
    if(filter_source_) {
        filter_source_->setCoefficients(CalcCoeffs());
    }
}

double Filter::GetGainFactor() const
{
    return gain_;
}

void Filter::SetGainFactor(double gain)
{
    gain_ = gain;
    if(filter_source_) {
        filter_source_->setCoefficients(CalcCoeffs());
    }
}

juce::IIRCoefficients Filter::CalcCoeffs() const
{
    if(filter_mode_ == Mode::kLowPass) {
        return IIRCoefficients::makeLowPass(sampling_rate_, filter_frequency_);
    } else if(filter_mode_ == Mode::kHighPass) {
        return IIRCoefficients::makeHighPass(sampling_rate_, filter_frequency_);
    } else if(filter_mode_ == Mode::kLowShelf) {
        return IIRCoefficients::makeLowShelf(sampling_rate_, filter_frequency_, Q_, gain_);
    } else if(filter_mode_ == Mode::kHighShelf) {
        return IIRCoefficients::makeHighShelf(sampling_rate_, filter_frequency_, Q_, gain_);
    } else if(filter_mode_ == Mode::kPeaking) {
        return IIRCoefficients::makePeakFilter(sampling_rate_, filter_frequency_, Q_, gain_);
    }
    
    assert(false);
    return IIRCoefficients();
}

void Filter::SetInputAudioSource(AudioSource *inputSource)
{
    filter_source_ = new IIRFilterAudioSource(inputSource, false);
}

//! override
void Filter::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    sampling_rate_ = sampleRate;
}

void Filter::releaseResources()
{
    
}

void Filter::getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill)
{
    if(!filter_source_) { return; }
    filter_source_->getNextAudioBlock(bufferToFill);
}

Equalizer::Equalizer()
:   source_(nullptr)
,   block_size_(256)
,   sampling_rate_(44100)
{
    hi_1_.SetFilterMode(Filter::Mode::kHighShelf);
    hi_1_.SetFrequency(1340);
    hi_1_.SetQ(0.56);
    hi_2_.SetFilterMode(Filter::Mode::kHighShelf);
    hi_2_.SetFrequency(1340);
    hi_2_.SetQ(0.56);
    
    mid_.SetFilterMode(Filter::Mode::kPeaking);
    mid_.SetFrequency(483);
    mid_.SetQ(0.38);
    
    lo_1_.SetFilterMode(Filter::Mode::kLowShelf);
    lo_1_.SetFrequency(176);
    lo_1_.SetQ(0.56);
    lo_2_.SetFilterMode(Filter::Mode::kLowShelf);
    lo_2_.SetFrequency(176);
    lo_2_.SetQ(0.56);
}

//! LowShelf, HighShelf, PeakingフィルターのGain Factor
double Equalizer::GetGain_High() const
{
    return hi_1_.GetGainFactor();
}

void Equalizer::SetGain_High(double gain)
{
    hi_1_.SetGainFactor(gain);
    hi_2_.SetGainFactor(gain);
}

double Equalizer::GetGain_Mid() const
{
    return mid_.GetGainFactor();
}

void Equalizer::SetGain_Mid(double gain)
{
    mid_.SetGainFactor(gain);
}

double Equalizer::GetGain_Low() const
{
    return lo_1_.GetGainFactor();
}

void Equalizer::SetGain_Low(double gain)
{
    lo_1_.SetGainFactor(gain);
    lo_2_.SetGainFactor(gain);
}

void Equalizer::SetAudioSource(AudioSource *inputSource, bool deleteInputWhenDeleted)
{
    std::unique_lock<std::mutex> lock(set_source_mutex_);
    
    if(source_ && deleteInputWhenDeleted_) {
        delete source_;
    }
    
    source_ = inputSource;
    deleteInputWhenDeleted_ = deleteInputWhenDeleted;
    
    hi_1_.SetInputAudioSource(source_);
    hi_2_.SetInputAudioSource(&hi_1_);
    mid_.SetInputAudioSource(&hi_2_);
    lo_1_.SetInputAudioSource(&mid_);
    lo_2_.SetInputAudioSource(&lo_1_);
    
    hi_1_.prepareToPlay(block_size_, sampling_rate_);
    hi_2_.prepareToPlay(block_size_, sampling_rate_);
    mid_.prepareToPlay(block_size_, sampling_rate_);
    lo_1_.prepareToPlay(block_size_, sampling_rate_);
    lo_2_.prepareToPlay(block_size_, sampling_rate_);
}

//! override
void Equalizer::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    block_size_ = samplesPerBlockExpected;
    sampling_rate_ = sampleRate;
}

void Equalizer::releaseResources()
{
    
}

void Equalizer::getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill)
{
    std::unique_lock<std::mutex> lock(set_source_mutex_);
    if(source_) {
        lo_2_.getNextAudioBlock(bufferToFill);
    } else {
        bufferToFill.clearActiveBufferRegion();
    }
}

TimeStretchedAudioSource::TimeStretchedAudioSource()
:   source_(nullptr)
,   deleteInputWhenDeleted_(false)
,   original_tempo_(120.0)
,   playback_tempo_(120.0)
,   sampling_rate_(44100)
,   block_size_(256)
,   accel_update_after_(kAccelUpdateInterval)
,   accel_rate_(1.0)
,   playback_position_phase_(0)
,   keep_pitch_(true)
{
    input_buffer_channel_info_.buffer = &input_buffer_;
    input_buffer_channel_info_.numSamples = 0;
    input_buffer_channel_info_.startSample = 0;
}

void    TimeStretchedAudioSource::SetAudioSource(juce::PositionableAudioSource *inputSource, bool deleteInputWhenDeleted, int numChannels)
{
    if(resampler_) { resampler_ = nullptr; }
    
    if(source_ && deleteInputWhenDeleted_) {
        delete source_;
        source_ = nullptr;
    }
    
    source_ = inputSource;
    deleteInputWhenDeleted_ = deleteInputWhenDeleted;
    
    if(source_) {
        resampler_ = new ResamplingAudioSource(source_, false, numChannels);
        source_->prepareToPlay(block_size_, sampling_rate_);
        resampler_->prepareToPlay(block_size_, sampling_rate_);
        sound_touch_.setSampleRate(sampling_rate_);
        sound_touch_.adjustAmountOfSamples(block_size_);
        sound_touch_.setChannels(numChannels);
        sound_touch_buffer_.resize(numChannels * block_size_);
        num_channels_ = numChannels;
        sound_touch_.setSetting(SETTING_USE_AA_FILTER, 1);
        sound_touch_.setSetting(SETTING_USE_QUICKSEEK, 0);
        
        input_buffer_.setSize(numChannels, block_size_);
    }
}

void 	TimeStretchedAudioSource::setNextReadPosition (int64 newPosition)
{
    if(!source_) { return; }
    source_->setNextReadPosition(StretchedSamplePosToOriginalSamplePos(newPosition));
}

int64 	TimeStretchedAudioSource::getNextReadPosition () const
{
    if(!source_) { return 0; }
    return OriginalSamplePosToStretchedSamplePos(source_->getNextReadPosition());
}

int64 	TimeStretchedAudioSource::getTotalLength () const
{
    if(!source_) { return 0; }
    return OriginalSamplePosToStretchedSamplePos(source_->getTotalLength());
}

bool 	TimeStretchedAudioSource::isLooping () const
{
    if(!source_) { return false; }
    return source_->isLooping();
}

void 	TimeStretchedAudioSource::setLooping (bool shouldLoop)
{
    if(!source_) { return; }
    source_->setLooping(shouldLoop);
}

bool    TimeStretchedAudioSource::IsPitchKeeped() const
{
    return keep_pitch_;
}

void    TimeStretchedAudioSource::KeepPitch(bool enable)
{
    keep_pitch_ = enable;
    ApplyTempoAndRate();
}

//! GetPlayback Tempo
float TimeStretchedAudioSource::GetPlaybackTempo() const
{
    return playback_tempo_;
}

//! Set Playback Tempo
void TimeStretchedAudioSource::SetPlaybackTempo(float tempo)
{
    playback_tempo_ = tempo;
    if(resampler_) {
        ApplyTempoAndRate();
    }
}

//! @return the file tempo.
float TimeStretchedAudioSource::GetOriginalTempo()
{
    return original_tempo_;
}

//! set file tempo.
void TimeStretchedAudioSource::SetOriginalTempo(float tempo)
{
    original_tempo_ = tempo;
    if(resampler_) {
        ApplyTempoAndRate();
    }
}

void TimeStretchedAudioSource::ApplyTempoAndRate()
{
    auto lock = get_unique_lock(accel_rate_mutex_);
    if(keep_pitch_) {
        sound_touch_.setTempo(playback_tempo_ / original_tempo_ * accel_rate_);
    } else {
        sound_touch_.setRate(playback_tempo_ / original_tempo_ * accel_rate_);        
    }
}

//! (1.0 + x)倍のスピードで再生。
void TimeStretchedAudioSource::AcceleratePlaybackSpeed(float acceleration_ratio)
{
    auto lock = get_unique_lock(accel_rate_mutex_);
    
    accel_rate_ *= (1.0 + acceleration_ratio);
    if(accel_rate_ < 0.1) {
        accel_rate_ = 0.1;
    } else if(accel_rate_ > 10.0) {
        accel_rate_ = 10.0;
    }
}

float TimeStretchedAudioSource::GetAcceleratedPlaybackSpeed() const
{
    auto lock = get_unique_lock(accel_rate_mutex_);
    return accel_rate_;
}

int TimeStretchedAudioSource::StretchedSamplePosToOriginalSamplePos(int pos) const
{
    return (int)(pos * original_tempo_ / playback_tempo_ + 0.5);
}

int TimeStretchedAudioSource::OriginalSamplePosToStretchedSamplePos(int pos) const
{
    return (int)(pos * playback_tempo_ / original_tempo_ + 0.5);
}

//! override
void TimeStretchedAudioSource::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    block_size_ = samplesPerBlockExpected;
    sampling_rate_ = sampleRate;
    playback_position_phase_ = 0;
    
    if(source_) {
        source_->prepareToPlay(samplesPerBlockExpected, sampleRate);
        resampler_->prepareToPlay(samplesPerBlockExpected, sampleRate);
        sound_touch_.setSampleRate(sampling_rate_);
        sound_touch_.adjustAmountOfSamples(block_size_);
        sound_touch_buffer_.resize(num_channels_ * block_size_);
        
        input_buffer_.setSize(input_buffer_.getNumChannels(), samplesPerBlockExpected);
    }
}

void TimeStretchedAudioSource::releaseResources()
{
    
}

void TimeStretchedAudioSource::getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill)
{
    if(!source_) {
        bufferToFill.clearActiveBufferRegion();
        return;
    }
    
    int processed_sample_count = 0;
    int const num_total_samples = bufferToFill.numSamples;
    for( ; ; ) {
        if(processed_sample_count == num_total_samples) {
            break;
        }
        
        int num_to_process = 0;
        
        if(accel_update_after_ == 0) {
            if(accel_rate_ < 1.0) {
                accel_rate_ = std::min<double>(accel_rate_ * 1.05, 1.0);
            } else {
                accel_rate_ = std::max<double>(accel_rate_ / 1.05, 1.0);
            }
            
            ApplyTempoAndRate();
            accel_update_after_ = kAccelUpdateInterval;
        }
        
        num_to_process = std::min<int>(accel_update_after_, bufferToFill.numSamples);
        double const num_to_process_in_source_hr = (num_to_process + playback_position_phase_) * (playback_tempo_ / original_tempo_ * accel_rate_);
        int num_to_process_in_source = (int)num_to_process_in_source_hr;
        
        playback_position_phase_ = num_to_process_in_source_hr - num_to_process_in_source;
        
        while(num_to_process_in_source != 0) {
            auto const n = std::min<int>(num_to_process_in_source, block_size_);
            input_buffer_channel_info_.numSamples = n;
            source_->getNextAudioBlock(input_buffer_channel_info_);
            num_to_process_in_source -= n;

            for(int ch = 0; ch < num_channels_; ++ch) {
                auto src = input_buffer_channel_info_.buffer->getReadPointer(ch);
                for(int smp = 0; smp < n; ++smp) {
                    sound_touch_buffer_[smp * num_channels_ + ch] = src[smp];
                }
            }
            
            auto lock = get_unique_lock(accel_rate_mutex_);
            sound_touch_.putSamples(sound_touch_buffer_.data(), n);
        }
        
        {
            auto lock = get_unique_lock(accel_rate_mutex_);
            sound_touch_.receiveSamples(sound_touch_buffer_.data(), num_to_process);
        }
        
        for(int ch = 0; ch < num_channels_; ++ch) {
            auto dest = bufferToFill.buffer->getWritePointer(ch);
            for(int smp = 0; smp < num_to_process; ++smp) {
                dest[smp + bufferToFill.startSample + processed_sample_count] = sound_touch_buffer_[smp * num_channels_ + ch];
            }
        }

        accel_update_after_ -= num_to_process;
        processed_sample_count += num_to_process;
    }
}

Deck::Deck()
:   sampling_rate_(44100)
,   block_size_(256)
,   deck_gain_(1.0)
{
}

//! load a file into the deck
bool Deck::LoadAudioFile(juce::File audio_file_path)
{
    juce::AudioFormatManager mgr;
    mgr.registerBasicFormats();
    auto reader = mgr.createReaderFor(audio_file_path);
    if(!reader) { return false; }
    
    loaded_file_ = audio_file_path;

    auto lock = get_unique_lock(mutex_);
    
    stretch_.SetAudioSource(nullptr, false, 0);
    
    if(transport_source_) {
        transport_source_->setSource(nullptr);
    }
    
    file_source_ = new juce::AudioFormatReaderSource(reader, true);
    file_source_->prepareToPlay(block_size_, sampling_rate_);
    
    transport_source_ = new juce::AudioTransportSource();
    transport_source_->prepareToPlay(block_size_, sampling_rate_);
    transport_source_->setSource(file_source_.get(), 0, nullptr, reader->sampleRate, reader->numChannels);
    transport_source_->setGain(0.5 * cos(0.25 * juce::float_Pi));
    
    stretch_.SetAudioSource(transport_source_.get(), false, reader->numChannels);
    stretch_.prepareToPlay(block_size_, sampling_rate_);
    transport_source_->prepareToPlay(block_size_, sampling_rate_);
    
    eq_.SetAudioSource(&stretch_, false);
    return true;
}

bool Deck::IsFileLoaded() const
{
    auto lock = get_unique_lock(mutex_);
    return transport_source_;
}

juce::File Deck::GetLoadedFile() const
{
    return loaded_file_;
}

double Deck::GetPlaybackPosition() const
{
    auto lock = get_unique_lock(mutex_);
    if(!transport_source_) { return 0.0; }
    
    return stretch_.getNextReadPosition() / (double)stretch_.getTotalLength();
}

void Deck::SetPlaybackPosition(double pos)
{
    auto lock = get_unique_lock(mutex_);
    if(!transport_source_) { return; }
    
    assert(pos >= 0 && pos <= 1.0);
    
    auto next = pos * (double)stretch_.getTotalLength();
    stretch_.setNextReadPosition(next);
}

void Deck::Play()
{
    if(!transport_source_) { return; }
    transport_source_->start();
}

void Deck::Pause()
{
    if(!transport_source_) { return; }
    transport_source_->stop();
}

bool Deck::IsPlaying() const
{
    if(!transport_source_) { return false; }
    return transport_source_->isPlaying();
}

void Deck::SetChangeTransportCallback(change_transport_status_callback callback)
{
    auto lock = get_unique_lock(mutex_);
    callback_ = callback;
}

void Deck::ClearChangeTransportCallback()
{
    auto lock = get_unique_lock(mutex_);
    callback_ = nullptr;
}

double  Deck::GetGain() const
{
    if(!transport_source_) { return 0.5; }
    return transport_source_->getGain();
}

void    Deck::SetGain(double gain)
{
    if(!transport_source_) { return; }

    transport_source_->setGain(gain);
}

void Deck::AcceleratePlaybackSpeed(float x)
{
    stretch_.AcceleratePlaybackSpeed(x);
}

float Deck::GetAcceleratedPlaybackSpeed() const
{
    return stretch_.GetAcceleratedPlaybackSpeed();
}

//! GetPlayback Tempo
float Deck::GetPlaybackTempo() const
{
    auto lock = get_unique_lock(mutex_);
    return stretch_.GetPlaybackTempo();
}

//! Set Playback Tempo
void Deck::SetPlaybackTempo(float tempo)
{
    auto lock = get_unique_lock(mutex_);
    stretch_.SetPlaybackTempo(tempo);
}

//! @return the file tempo.
float Deck::GetFileTempo()
{
    auto lock = get_unique_lock(mutex_);
    return stretch_.GetOriginalTempo();
}

//! set file tempo.
void Deck::SetFileTempo(float tempo)
{
    auto lock = get_unique_lock(mutex_);
    return stretch_.SetOriginalTempo(tempo);
}

//! override
void Deck::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    auto lock = get_unique_lock(mutex_);
    
    sampling_rate_ = sampleRate;
    block_size_ = samplesPerBlockExpected;
    
    if(transport_source_) {
        file_source_->prepareToPlay(samplesPerBlockExpected, sampleRate);
        transport_source_->prepareToPlay(samplesPerBlockExpected, sampleRate);
        stretch_.prepareToPlay(samplesPerBlockExpected, sampleRate);
        eq_.prepareToPlay(samplesPerBlockExpected, sampleRate);
    }
}

void Deck::releaseResources()
{
    
}

void Deck::getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill)
{
    if(!IsFileLoaded()) {
        bufferToFill.clearActiveBufferRegion();
        return;
    }
 
    auto lock = get_unique_lock(mutex_);
    eq_.getNextAudioBlock(bufferToFill);
    
    if(stretch_.getNextReadPosition() / (double)stretch_.getTotalLength() >= 1) {
        stretch_.setNextReadPosition(stretch_.getTotalLength());
        if(callback_) {
            callback_(TransportChangeStatus::kStopped);
        }
    }
}

Equalizer & Deck::GetEqualizer()
{
    return eq_;
}

Equalizer const & Deck::GetEqualizer() const
{
    return eq_;
}

Effector::Effector()
:   block_size_(256)
,   sampling_rate_(44100)
,   dry_gain_(1)
,   wet_gain_(0)
,   last_dry_gain_(1)
,   last_wet_gain_(0)
{
    plugin_format_manager_.addDefaultFormats();
}

Effector::~Effector()
{}

bool Effector::LoadPlugin(juce::File plugin_path)
{
    bool successful = false;
    juce::OwnedArray<juce::PluginDescription> types;
    
    for(int nf = 0; nf < plugin_format_manager_.getNumFormats(); ++nf) {
        successful = plugin_list_.scanAndAddFile(plugin_path.getFullPathName(), true, types, *plugin_format_manager_.getFormat(nf));
        if(successful) { break; }
    }
    
    if(!successful || types.size() == 0) {
        return false;
    }
    
    auto &plugin_description = *types[0];
    
    auto lock = get_unique_lock(proc_mutex_);
    
    juce::String error_message;
    auto instance = plugin_format_manager_.createPluginInstance(plugin_description, sampling_rate_, block_size_, error_message);
    if(!instance) {
        return false;
    }
    
    proc_ = instance;
    proc_->prepareToPlay(sampling_rate_, block_size_);
    proc_->suspendProcessing(false);
    effect_buffer_.setSize(std::max<int>(plugin_description.numInputChannels, plugin_description.numOutputChannels), block_size_);
    
    return true;
}

juce::String Effector::GetPluginName() const
{
    return proc_->getName();
}

bool Effector::IsPluginLoaded() const
{
    return proc_;
}

bool Effector::HasEditor() const
{
    if(!proc_) { return false; }
    
    return proc_->hasEditor();
}

juce::AudioProcessorEditor * Effector::CreateEditor()
{
    if(HasEditor()) {
        return proc_->createEditor();
    }
    return nullptr;
}

int Effector::GetProgramCount() const
{
    if(!proc_) { return 0; }
    return proc_->getNumPrograms();
}

juce::String Effector::GetProgramName(int index) const
{
    if(!proc_) { return ""; }
    return proc_->getProgramName(index);
}

void Effector::SetProgram(int index)
{
    proc_->setCurrentProgram(index);
}

int Effector::GetParameterCount() const
{
    if(!proc_) { return 0; }
    return proc_->getNumParameters();
}

juce::String Effector::GetParameterName(int index) const
{
    if(!proc_) { return ""; }
    
    return proc_->getParameterName(index);
}

float Effector::GetParameterValue(int index) const
{
    if(!proc_) { return 0; }
    
    return proc_->getParameter(index);
}

void Effector::SetParameterValue(int index, float value)
{
    if(!proc_) { return; }
    
    proc_->setParameter(index, value);
}

void Effector::SetDryWetRatio(float ratio)
{
    dry_wet_ratio_ = ratio;
    dry_gain_ = cos(ratio * 0.5 * juce::float_Pi);
    wet_gain_ = cos((1.0 - ratio) * 0.5 * juce::float_Pi);
}

void Effector::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    block_size_ = samplesPerBlockExpected;
    sampling_rate_ = sampleRate;
    
    if(proc_) {
        proc_->prepareToPlay(samplesPerBlockExpected, sampleRate);
        effect_buffer_.setSize(effect_buffer_.getNumChannels(), block_size_);
    }
}

void Effector::releaseResources()
{
}

void Effector::getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill)
{
    auto lock = get_unique_lock(proc_mutex_);
    if(!proc_) {
        return;
    }
    
    int const channels = std::min<int>(bufferToFill.buffer->getNumChannels(), effect_buffer_.getNumChannels());
    for(int ch = 0; ch < channels; ++ch) {
        effect_buffer_.copyFrom(ch, 0, *bufferToFill.buffer, ch, bufferToFill.startSample, bufferToFill.numSamples);
    }
    int const to_be_cleared = effect_buffer_.getNumChannels() - channels;
    for(int ch = channels; ch < to_be_cleared; ++ch) {
        effect_buffer_.clear(ch, 0, effect_buffer_.getNumSamples());
    }
    
    juce::MidiBuffer m;
    proc_->processBlock(effect_buffer_, m);

    bool const gain_ratio_has_changed = (last_dry_gain_ != dry_gain_);
    if(gain_ratio_has_changed) {
        for(int ch = 0; ch < channels; ++ch) {
            bufferToFill.buffer->applyGainRamp(ch, 0, bufferToFill.numSamples, last_dry_gain_, dry_gain_);
            bufferToFill.buffer->addFromWithRamp(ch, bufferToFill.startSample, effect_buffer_.getReadPointer(ch), bufferToFill.numSamples, last_wet_gain_, wet_gain_);
        }
    } else {
        for(int ch = 0; ch < channels; ++ch) {
            bufferToFill.buffer->applyGain(ch, 0, bufferToFill.numSamples, dry_gain_);
            bufferToFill.buffer->addFrom(ch, bufferToFill.startSample, effect_buffer_.getReadPointer(ch), bufferToFill.numSamples, wet_gain_);
        }
    }
    last_dry_gain_ = dry_gain_;
    last_wet_gain_ = wet_gain_;
}

Mixer::Mixer()
{}

void Mixer::SetDecks(Deck &deck_a, Deck &deck_b)
{
    mixer_.removeAllInputs();

    deck_a_ = &deck_a;
    deck_b_ = &deck_b;
    
    mixer_.addInputSource(deck_a_, false);
    mixer_.addInputSource(deck_b_, false);
}

Effector & Mixer::GetEffector()
{
    return effector_;
}

Effector const & Mixer::GetEffector() const
{
    return effector_;
}

void Mixer::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    mixer_.prepareToPlay(samplesPerBlockExpected, sampleRate);
    effector_.prepareToPlay(samplesPerBlockExpected, sampleRate);
}

void Mixer::releaseResources()
{
    mixer_.releaseResources();
    effector_.releaseResources();
}

void Mixer::getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill)
{
    mixer_.getNextAudioBlock(bufferToFill);
    effector_.getNextAudioBlock(bufferToFill);
}