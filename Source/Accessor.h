/*
  ==============================================================================

    Accessor.h
    Created: 3 Sep 2015 8:54:26pm
    Author:  yuasa

  ==============================================================================
*/

#ifndef ACCESSOR_H_INCLUDED
#define ACCESSOR_H_INCLUDED

class Deck;
class Mixer;
class Effector;

Mixer & GetMixer();
Deck & GetDeckA();
Deck & GetDeckB();
Effector & GetEffector();

#endif  // ACCESSOR_H_INCLUDED
