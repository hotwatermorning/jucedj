/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef COMPONENTS_H_INCLUDED
#define COMPONENTS_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "./Models.h"
#include <thread>

class TempoSlider
:   public juce::Slider
{
public:
    TempoSlider (const String& componentName): JUCE_NAMESPACE::Slider(componentName) {};
    TempoSlider() {};
    
    void SetOriginalTempo(double original_tempo);
    
    double proportionOfLengthToValue (double proportion) override;
    double valueToProportionOfLength (double value) override;
    
private:
    double original_tempo_;
};

class WaveformDisplayComponent
:   public juce::Component
,   public juce::ChangeListener
,   public juce::ChangeBroadcaster
,   private juce::ScrollBar::Listener
,   private juce::Timer
,   private juce::ButtonListener
{
public:
    class OnPlayMessageListener
    {
    protected:
        OnPlayMessageListener() {}
    public:
        virtual ~OnPlayMessageListener() {}
        
        virtual void OnPlay() = 0;
    };
    
public:
    WaveformDisplayComponent()
    :   scrollbar (false)
    ,   thumbnailCache (5)
    ,   isFollowingTransport (true)
    ,   deck_(nullptr)
    ,   zoom_ratio_(0)
    ,   last_zoom_x_(-1)
    ,   last_zoom_y_(-1)
    {
        audioFormat.registerBasicFormats();
        thumbnail = new juce::AudioThumbnail(512, audioFormat, thumbnailCache);
        thumbnail->addChangeListener (this);
        
        addAndMakeVisible(scrollbar);
        scrollbar.setRangeLimits(visibleRange);
        scrollbar.setAutoHide(false);
        scrollbar.addListener(this);
        
        currentPositionMarker.setFill(Colours::white.withAlpha (0.85f));
        addAndMakeVisible(currentPositionMarker);
        
        addAndMakeVisible(transport_switch_button_);
        transport_switch_button_.addListener(this);
        transport_switch_button_.setClickingTogglesState(true);
        transport_switch_button_.setToggleState(true, juce::NotificationType::dontSendNotification);
        transport_switch_button_.setButtonText("Follow");
    }
    
    ~WaveformDisplayComponent()
    {
        scrollbar.removeListener (this);
        thumbnail->removeChangeListener (this);
    }
    
    juce::Array<OnPlayMessageListener *> listeners_;
    
    void AddPlaybackListener(OnPlayMessageListener *listener)
    {
        listeners_.add(listener);
    }
    
    void RemovePlaybackListener(OnPlayMessageListener *listener)
    {
        listeners_.removeFirstMatchingValue(listener);
    }
    
    void ClearAllPlaybackListener()
    {
        listeners_.clear();
    }
    
    void SetDeck(Deck &deck)
    {
        deck_ = &deck;
    }
    
    void setFile (const File& file)
    {
        if (! file.isDirectory())
        {
            thumbnail->setSource (new FileInputSource (file));
            const Range<double> newRange (0.0, thumbnail->getTotalLength());
            scrollbar.setRangeLimits (newRange);
            setRange (newRange);
            
            startTimerHz (60);
        }
    }
    File getLastDroppedFile() const noexcept                    { return lastFileDropped; }
    
    void setZoomFactor (double amount, double timeAtCentre, double centre_point)
    {
        if (thumbnail->getTotalLength() > 0)
        {
            const double newScale = jmax (0.001, thumbnail->getTotalLength() * (1.0 - jlimit (0.0, 0.99, amount)));
            
            double min_value = timeAtCentre - newScale * centre_point;
            double max_value = timeAtCentre + newScale * (1 - centre_point);
            
            if(!isFollowingTransport) {
                if(min_value < 0) {
                    max_value += -min_value;
                    min_value = 0;
                }
                
                if(max_value > thumbnail->getTotalLength()) {
                    min_value -= (max_value - thumbnail->getTotalLength());
                    max_value = thumbnail->getTotalLength();
                }
            }
            
            setRange (Range<double> (min_value, max_value));
        }
    }
    
    void setRange (Range<double> newRange)
    {
        visibleRange = newRange;
        scrollbar.setCurrentRange (visibleRange);
        updateCursorPosition();
        repaint();
    }
    
    void setFollowsTransport (bool shouldFollow)
    {
        isFollowingTransport = shouldFollow;
    }
    
    void paint (Graphics& g) override
    {
        g.fillAll (Colours::darkgrey);
        g.setColour (Colours::lightblue);
        
        if (thumbnail->getTotalLength() > 0.0)
        {
            Rectangle<int> thumbArea (getLocalBounds());
            thumbArea.removeFromBottom (scrollbar.getHeight() + 4);
            thumbnail->drawChannels (g, thumbArea.reduced (2),
                                    visibleRange.getStart(), visibleRange.getEnd(), 1.0f);
        }
        else
        {
            g.setFont (14.0f);
            g.drawFittedText ("(No audio file selected)", getLocalBounds(), Justification::centred, 2);
        }
    }
    
    void resized() override
    {
        scrollbar.setBounds (getLocalBounds().removeFromBottom (14).reduced (2));
        
        int const kSwitchHeight = 20;
        int const kSwitchWidth = 60;
        auto bounds = getLocalBounds();
        bounds = bounds.removeFromBottom(kSwitchHeight);
        bounds = bounds.removeFromLeft(kSwitchWidth);
        transport_switch_button_.setBounds(bounds);
    }
 
    void buttonClicked(juce::Button *btn) override
    {
        if(btn == &transport_switch_button_) {
            isFollowingTransport = transport_switch_button_.getToggleState();
        }
    }
    
    void changeListenerCallback (ChangeBroadcaster*) override
    {
        // this method is called by the thumbnail when it has changed, so we should repaint it..
        repaint();
    }
    
    void mouseDown (const MouseEvent& e) override
    {
        mouseDrag (e);
    }
    
    void mouseDrag (const MouseEvent& e) override
    {
        if (canMoveTransport()) {
            double pos = xToTime((float)e.x) / thumbnail->getTotalLength();
            deck_->SetPlaybackPosition(jmax (0.0, pos));
        }
    }
    
    void mouseUp (const MouseEvent&) override
    {
        for(auto listener: listeners_) {
            listener->OnPlay();
        }
    }
    
    void mouseWheelMove (const MouseEvent& e, const MouseWheelDetails& wheel) override
    {
        if (thumbnail->getTotalLength() > 0.0)
        {
            double newStart = visibleRange.getStart() - wheel.deltaX * (visibleRange.getLength()) / 10.0;
            newStart = jlimit (0.0, jmax (0.0, thumbnail->getTotalLength() - (visibleRange.getLength())), newStart);
            
            if (canMoveTransport()) {
                setRange (Range<double> (newStart, newStart + visibleRange.getLength()));
            }
            
            if (wheel.deltaY != 0.0f) {
                if(isFollowingTransport) {
                    last_zoomed_playback_position_in_sec_ = GetPlaybackPositionInSeconds();
                    last_zoomed_centre_point_ = 0.5;
                } else {
                    if(!wheel.isInertial && (last_zoom_x_ != e.x || last_zoom_y_ != e.y)) {
                        last_zoomed_playback_position_in_sec_ = xToTime(e.x);
                        last_zoomed_centre_point_ = e.x / (double)getWidth();
                    }
                }
                last_zoom_x_ = e.x;
                last_zoom_y_ = e.y;
                
                auto last_zoom_ratio = zoom_ratio_;
                zoom_ratio_ -= wheel.deltaY;
                if(zoom_ratio_ < 0) { zoom_ratio_ = 0; }
                if(zoom_ratio_ > 1.0) { zoom_ratio_ = 1.0; }
                
                if(zoom_ratio_ != last_zoom_ratio) {
                    setZoomFactor(zoom_ratio_, last_zoomed_playback_position_in_sec_, last_zoomed_centre_point_);
                }
            }
            
            repaint();
        }
    }
    
private:
    ScrollBar scrollbar;
    double zoom_ratio_;
    double last_zoomed_playback_position_in_sec_;
    double last_zoomed_centre_point_;
    int last_zoom_x_;
    int last_zoom_y_;
    Deck *deck_;
    juce::TextButton transport_switch_button_;
    
    juce::AudioThumbnailCache thumbnailCache;
    juce::AudioFormatManager audioFormat;
    juce::ScopedPointer<juce::AudioThumbnail> thumbnail;
    Range<double> visibleRange;
    bool isFollowingTransport;
    File lastFileDropped;
    
    DrawableRectangle currentPositionMarker;
    
    float timeToX (const double time) const
    {
        return getWidth() * (float) ((time - visibleRange.getStart()) / (visibleRange.getLength()));
    }
    
    double xToTime (const float x) const
    {
        return (x / getWidth()) * (visibleRange.getLength()) + visibleRange.getStart();
    }
    
    bool canMoveTransport() const noexcept
    {
        return ! (isFollowingTransport && deck_->IsPlaying());
    }
    
    void scrollBarMoved (ScrollBar* scrollBarThatHasMoved, double newRangeStart) override
    {
        if (scrollBarThatHasMoved == &scrollbar)
            if (! (isFollowingTransport && deck_->IsPlaying()))
                setRange (visibleRange.movedToStartAt (newRangeStart));
    }
    
    //! 秒単位での再生位置を取得
    double GetPlaybackPositionInSeconds() const
    {
        return deck_->GetPlaybackPosition() * thumbnail->getTotalLength();
    }
    
    void timerCallback() override
    {
        if(canMoveTransport()) {
            updateCursorPosition();
        } else {
            setRange(visibleRange.movedToStartAt (GetPlaybackPositionInSeconds() - (visibleRange.getLength() / 2.0)));
        }
    }
    
    void updateCursorPosition()
    {
        currentPositionMarker.setVisible (thumbnail->getTotalLength() > 0.0);
        currentPositionMarker.setRectangle (Rectangle<float> (timeToX (GetPlaybackPositionInSeconds()) - 0.75f, 0,
                                                              1.5f, (float) (getHeight() - scrollbar.getHeight())));
    }
};

class TempoSliderComponent
:   public juce::Component
,   public juce::Slider::Listener
{
public:
    TempoSliderComponent();
    
    void SetDeck(Deck &deck);
    void UpdateTempoValue();
    
private:
    void resized() override;
    void sliderValueChanged(Slider *slider) override;
    
private:
    Deck *deck_;
    TempoSlider tempo_slider_;
};

class TransportComponent
:   public juce::Component
,   public juce::ButtonListener
,   public juce::MessageListener
,   private WaveformDisplayComponent::OnPlayMessageListener
{
public:
    TransportComponent();
    void SetDeck(Deck &deck);

    void OnFileLoad();
    void OnChangeTransport(Deck::TransportChangeStatus status);
    void OnPlay() override;
    
    class DeckTransportChangeMessage;
    
private:
    void resized() override;
    void paint(juce::Graphics &) override;
    void buttonClicked(juce::Button *btn) override;
    void handleMessage (const Message &message) override;
    
    juce::Image play_button_image_;
    juce::ImageButton play_button_;
    Deck *deck_;
};

class TurnTableComponent
:   public Component
,   public juce::Timer
,   public juce::ButtonListener
,   public juce::MessageListener
{
public:
    TurnTableComponent(bool left_hand_side);
    ~TurnTableComponent();
    
    void SetDeck(Deck &deck);
    void UpdateTempoSlider();
    
private:
    juce::Image record_;
    juce::Image record_stain_;
    Deck *deck_;
    bool left_hand_side_;
    juce::Label file_path_label_;
    juce::TextButton load_file_button_;
    juce::TextButton play_button_;
    std::thread bpm_detection_thread_;
    WaveformDisplayComponent waveform_;
    
    TempoSliderComponent tempo_slider_;
    TransportComponent transport_;
    
private:
    void timerCallback () override;
    void mouseDown(juce::MouseEvent const &event) override;
    void mouseWheelMove(juce::MouseEvent const &event, juce::MouseWheelDetails const &wheel) override;
    void paint (Graphics&) override;
    void resized() override;
    void buttonClicked(juce::Button *btn) override;
    void handleMessage (const Message &message) override;
    
    bool IsOnRecord(int x, int y);
    
    void LoadFile();
    void Play();
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TurnTableComponent)
};

class DeckMixerComponent
:   public juce::Component
,   public juce::Slider::Listener
{
private:
    juce::Slider slider_hi_;
    juce::Slider slider_mid_;
    juce::Slider slider_lo_;
    
    juce::Slider channel_volume_;
    Deck *deck_;
    
    //! dBではなく0.0 .. 2.0のリニア値
    std::function<void(double gain)> on_volume_changed_;
    
public:
    DeckMixerComponent(std::function<void(double gain)> on_volume_changed);
    
    void SetDeck(Deck &deck);
    
    double GetDeckVolume() const;
    
private:
    void resized() override;
    void paint(juce::Graphics &) override;
    void sliderValueChanged(Slider *slider) override;
};

class MixerComponent
:   public juce::Component
,   public juce::Slider::Listener
{
public:
    MixerComponent();
    
    void paint(Graphics &) override;
    void resized() override;
    void sliderValueChanged (Slider* slider) override;
    
private:
    juce::Slider channel_fader_;
    
    DeckMixerComponent deck_mixer_a_;
    DeckMixerComponent deck_mixer_b_;
    
    double CalcDeckAVolume();
    double CalcDeckBVolume();
    
    void ApplyChannelVolume(double gain, bool is_deck_a);
};

class EffectorComponent;

class XYPanelComponent
:   public juce::Component
,   public juce::FileDragAndDropTarget
{
public:
    XYPanelComponent();
    
    void SetOwnerComponent(EffectorComponent &owner);
  
    void SetX(float pos);
    void SetY(float pos);
private:
    void mouseDrag(MouseEvent const &event) override;
    void mouseDown(MouseEvent const &event) override;
    void mouseUp(MouseEvent const &event) override;
    void paint(juce::Graphics &) override;
    void resized() override;
    void filesDropped (const StringArray &files, int x, int y) override;
    bool isInterestedInFileDrag (const StringArray &files) override;
    
    bool dragging_;
    float x_pos_; //! -1.0 .. 1.0
    float y_pos_; //! -1.0 .. 1.0

    float original_x_pos_;
    float original_y_pos_;
    
    EffectorComponent *owner_;
    float knob_width_;
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (XYPanelComponent)
};

class PluginEditorWindow
:   public juce::DocumentWindow
,   public juce::ChangeBroadcaster
{
public:
    PluginEditorWindow(juce::String plugin_name);
    ~PluginEditorWindow();
    
    void SetEditor(juce::AudioProcessorEditor *editor);
    
    void closeButtonPressed() override;
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PluginEditorWindow)
};

class EffectorComponent
:   public juce::Component
,   public juce::ButtonListener
,   public juce::ChangeListener
,   public juce::ComboBoxListener
,   public juce::SliderListener
{
public:
    EffectorComponent();
    ~EffectorComponent();

    void OnValueChanged(float x, float y);
    void OnPluginDropped(juce::File plugin_path);
    
    bool IsPluginLoaded() const;
    
private:
    void paint(Graphics &) override;
    void resized() override;
    void buttonClicked(juce::Button *button) override;
    void changeListenerCallback (ChangeBroadcaster *source) override;
    void comboBoxChanged (ComboBox *comboBoxThatHasChanged) override;
    void sliderValueChanged(Slider *slider) override;
    
    void RemoveEditor();
    
    juce::Slider dry_wet_slider_;
    juce::ComboBox programs_list_;
    juce::ComboBox x_parameter_list_;
    juce::ComboBox y_parameter_list_;
    XYPanelComponent xy_panel_;
    juce::TextButton open_editor_;
    
    juce::ScopedPointer<PluginEditorWindow> plugin_editor_window_;
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (EffectorComponent)
};

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent
:   public juce::Component
{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();
    
    void PrepareToShutdown();
    
private:
    void paint (Graphics&) override;
    void resized() override;
    
    TurnTableComponent turn_table_a_;
    TurnTableComponent turn_table_b_;
    MixerComponent mixer_;
    
    juce::ScopedPointer<EffectorComponent> effector_;

private:
    void play_deck(bool for_deck_a);
    
private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // COMPONENTS_H_INCLUDED
