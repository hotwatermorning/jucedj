/* (Auto-generated binary data file). */

#ifndef BINARY_BINARYRESOURCE_H
#define BINARY_BINARYRESOURCE_H

namespace BinaryResource
{
    extern const char*  playback_button_png;
    const int           playback_button_pngSize = 78027;

    extern const char*  record_png;
    const int           record_pngSize = 202417;

    extern const char*  record_stain_png;
    const int           record_stain_pngSize = 77922;

}

#endif
