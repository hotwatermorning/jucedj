# このプログラムは

(Event for Diverse Game Engineers)[http://peatix.com/event/100971] のLT、「JUCEで作るオーディオアプリケーション」の発表用に用意した、JUCE製のDJアプリケーションのデモです。

テンポ変更のためにSoundTouchライブラリを使用していますので、Source/SoundTouch以下に、SoundTouch-1.9.1を解凍して配置してください。

hotwatermorning@gmail.com